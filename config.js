const {
	APP_SECRET,
	DB_CONN_STR,
	MAPBOX_API_KEY,
    USERNAME,
    PASSWORD
} = process.env

let config = {
	"appSecret": APP_SECRET || "appsecret",
	"port": 3000,
	"dbConnStr": DB_CONN_STR || "mongodb://127.0.0.1/vis",
	"googleApiKey": MAPBOX_API_KEY || "mykey",
	"username": USERNAME || false,
	"password": PASSWORD || false,
}

module.exports = config;
