# Scientific output visualiser

The scientific output visualiser is a tool for graphic representation
of meta-data about research, using D3,js, jQuery, and Node.
The tool uses output from the Expertus SPLENDOR system as it's intput data.

Wizualizator dorobku naukowego to narzędzie do graficznego przedstawiania metadanych o publikacjach naukowych.
Powstało z użyciem D3.js, jQuery oraz Node.
Dane wejściowe są pobierane z serwisu Expertus SPLENDOR.

## Uruchomienie aplikacji

Aplikacja została przygotowana jako obraz Dockerowy gotowy do uruchomienia z odpowiednimi parametrami. Do uruchomienia
należy wykonć następujące kroki.

1. Należy skopiować plik `.env.example` i  zapisać go jako `.env`
2. Należy założyć konto na portalu https://www.mapbox.com/ (pozwala na 200 000 zapytań miesięcznie za darmo a przy przekroczeniu limitu należy zapłacić) i skopiować klucz API (Default public token lub wygenerować nowy)
   i umieścić go pod zmienną  `MAPBOX_API_KEY` w pliku  `.env`
3. Do działania aplikacji potrzebna będzie również instancja bazy danych `MongoDB` (aplikacja była  testowana przeciwko  wersji `MongoDB 5.0.8`) do której połączenie w aplikacji powinno
   zostać skonfigurowane przez zmienną `DB_CONN_STR` w pliku `.env` przy pomocy następującego wzorca `mongodb://user:pass@host:port/vis`
4. Ostatnia zmienna, którą należy skonfigurować to wygenerowanie losowego ciągu znaków i zapisanie go pod zmienną `APP_SECRET`
5. Następnie należy pobrać obraz aplikacji z rejestra kontenerów na portalu gitlab pod następującym adresem https://gitlab.com/hotone/vis/container_registry przy użyciu nastęującej komendy
   ```
    docker pull registry.gitlab.com/hotone/vis:1.2.0
   ```
6. Aby uruchomić aplikację należy użyć następującej komendy
    ```
    docker run -d --name vis --env-file .env registry.gitlab.com/hotone/vis:1.2.0
    ```
   `Uwaga: aplikacja domyślnie eksponowana jest na porcie 3000`

   Zaleca się nie wystawiać jej bezpośrednio "na świat" tylko użyć dodatkowo rozwiązania reverse proxy (np. Nginx), które będzie pośredniczyć między aplikacją a klientami
7. Na samym końcu pozostaje  utworzyć użytkowników, na które istnieją dwa sposoby:
   1. Opcja interaktywna po wykonaniu której skrypt poprosi użytkownika o podanie nowego hasła i loginu.
      Ponowne uruchomienie skryptu doda kolejnego użytkownika, chyba, że podamy istniejący w bazie login -
      wtedy hasło odpowiadające temu loginowi będzie nadpisane.
       ``` 
       docker exec -it vis npm run createUser
       ```
   2. Opcja skryptowa pozwalająca przekazać  użytkownika i hasło  jako zmienne środowiskowe `USERNAME` oraz `PASSWORD`
       ```
       docker exec -it vis /bin/bash -c  'USERNAME=user1234 PASSWORD=12345 npm run createUser'
       ```
8. Aby sprawdzić logi aplikacji należy zastosować następującą komendę
    ```
    docker logs --follow vis
    ```



