var express = require('express');
var PDFDocument = require('pdfkit');
var svgToPdf = require('svg-to-pdfkit');
var fs = require('fs');
var router = express.Router();
var mg = require('mongodb');
var m = require('../utils/models');
var auth = require('../utils/auth')
var parser = require('../utils/expertus')
const logger = require("../utils/logger");


router.get('/', function (req, res, next) {

	m.Department.find().then(departments => {
		res.data.savedItems = departments.map(dept => {
			dept._id = dept._id.toString();
			return dept;
		});
		return res.render('admin/dashboard', res.data);
	})

})

router.post('/addSaved', function (req, res, next) {
	parser.upload(req, function (err, file, extension, form) {
		if(err) {
			logger.logError(err);
			return res.redirect('/error/parser');
		}
		if (!form.name) {
			return res.redirect('/error/missing_name')
		}
		if (!form.short_name) {
			return res.redirect('/error/missing_short_name')
		}

		parser.parse(file, function (err, works, queryName) {
			if(err) {
				logger.logError(err);
				return res.redirect('/error/parser');
			}
			var newDept = new m.Department({
				name: form.name,
				shortName : form.short_name,
				works: works
			})

			newDept.save().catch(console.log);

			res.redirect('/');
		})
	})
	logger.logInfo('path done');
});

router.get('/removeSaved', function (req, res, next) {
	logger.logInfo('removing: ' + req.query.name);
	m.Department.remove({_id: req.query.name}).then();

	res.redirect('/admin');

})

router.get('/setShortName', function (req, res) {
	var newName = req.query.new, fullName = req.query.name;
	if(newName && fullName) {
		m.findOne({name: fullName}, function (err, result) {
			if(err) logger.logError(err)
			m.shortName = newName;
			m.save();
		})
	}
})

module.exports = router;
