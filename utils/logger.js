function log(prefix, message){
    console.log(`[${new Date().toISOString()}] [${prefix}] ${message}`);
}

class Logger {

    static logInfo(message) {
        log('INFO', message);
    };
    static logWarn(message){
        log('WARN', message);
    };
    static logError(message){
        log('ERROR', message);
    };
}

module.exports = Logger