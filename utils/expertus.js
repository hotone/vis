const cities = require('../resources/cities.json');
const journals = require('../resources/journals.json');
let Busboy = require('busboy');
let iconv = require('iconv-lite');
const parseRTF = require('rtf-parser')
const fs = require('fs')
const logger = require("./logger");

function parseFileExtension(filename) {
    return String(filename).split('.').pop()
}

function splitRtfDocToRawRecordsArray(rtfDoc) {
    const beginningOfNewRecordRegex = new RegExp(/^\d+\./);
    let rawRecords = []
    let currentRecord = []
    rtfDoc.content.forEach((rtfParagraph) => {
        if (rtfParagraph.content.length !== 0) {
            if (beginningOfNewRecordRegex.test(rtfParagraph.content[0].value)) {
                if (currentRecord.length !== 0)
                    rawRecords.push(currentRecord)
                currentRecord = []
            }
            currentRecord.push(rtfParagraph)
        }
    })
    rawRecords.push(currentRecord)
    currentRecord = []
    return rawRecords
}

let parser = {};

parser.upload = function (request, callback) {
    let busboy = new Busboy({headers: request.headers});
    let chunks = [];
    let error = null;
    let form = {};
    busboy.on('field', function (fieldname, value) {
        form[fieldname] = value;
    });
    busboy.on('file', function (fieldname, file, filename, encoding, mimetype) {
        let extension = parseFileExtension(filename)

        file.on('data', function (chunk) {
            chunks.push(chunk);
        });
        file.on('end', function () {

            if (String(extension).toLowerCase() === "csv") {
                callback(null, iconv.decode(Buffer.concat(chunks), "UTF-8"), extension, form)
            }
            if (String(extension).toLowerCase() === "rtf") {
                callback(null, iconv.decode(Buffer.concat(chunks), "UTF-8"), extension, filename, form)
            } else {
                file.on('error', callback)
            }
        })
        file.on('error', callback)
    });
    busboy.on('finish', function () {

    })
    request.pipe(busboy)
}


function extractRecordProperties(rawRecord) {
    let recordProperties = []
    rawRecord.forEach((rawParagraph) => {
        let str = ""
        rawParagraph.content.forEach((rtfSpan) => {
            let value = rtfSpan.value
            if (value.startsWith("*")) {
                value = value.substring(1)
            }
            str = str.concat(value)
        })
        const [key, ...rest] = str.split(':')
        const value = rest.join(':')
        recordProperties.push({key, value})
    })
    return recordProperties
}

function checkIsThisYearOrPotentialCity(potentialCityName) {
    const fourDigitYearRegex = new RegExp(/\d{4}/);
    return fourDigitYearRegex.test(
        potentialCityName
    ) ?
        null :
        potentialCityName;
}

function createRecordObjectFromRawRecord(rawRecord) {
    let recordObject = {
        authorsExpertusFormat: [],
        languages: [],
        polishkeywords: [],
        englishkeywords: [],
        authors: [],
        invalid: {}
    };

    let pageCountTowardsSumConditions = {
        typFormalny: false,
        typMerytoryczny: false,
        czyWycinek: false
    }

    let recordProperties = extractRecordProperties(rawRecord)

    recordObject.country = null;
    recordObject.points = 0;
    recordObject.impactFactor = 0;
    recordObject.openAccessArticle = false;
    recordObject.conferencePaper = false;
    recordObject.disciplines = []

    recordProperties.forEach((property) => {

        if (property.key.toLowerCase().includes('Dyscypliny czasopisma na podstawie wykazu MNiSW'.toLowerCase())){
            recordObject.disciplines = property.value.split(';')
        }
        switch (property.key) {
            case 'Autorzy':
                property.value.split(',').forEach((name) => {
                    name = name.trim();
                    name = name.replace(/\./g, "");
                    recordObject.authorsExpertusFormat.push(name);
                    recordObject.authors.push(name);
                })
                break;
            case 'Tytuł oryginału':
                recordObject.title = property.value;
                break;
            case 'Tytuł równoległy':
                recordObject.titleVariant = property.value;
                break;
            case 'Adres wydawniczy':
                const potentialCityName = property.value.split(',')[0]
                    .trim()
                    .replace('[', '')
                    .replace(']', '')
                recordObject.potentialCity = checkIsThisYearOrPotentialCity(potentialCityName)
                if (cities[recordObject.potentialCity]) {
                    recordObject.city = recordObject.potentialCity;
                    recordObject.latLng = cities[recordObject.potentialCity]
                    if (recordObject.latLng !== undefined)
                        recordObject.country = recordObject.latLng.country
                }
                break;
            case 'Rok':
                recordObject.year = Number.parseInt(property.value);
                break;
            case 'Charakt. formalna':
                if (property.value.toString().trim() === "fragment/rozdział ze zbiorówki"){
                    recordObject.publicationCharacteristics = "chapter_in_book"
                }
                else if (property.value.toString().trim() === "książka (autor lub redakcja)"){
                    recordObject.publicationCharacteristics = "book"
                }
                else if (property.value.toString().trim() === "artykuł/recenzja z czasopisma"){
                    recordObject.publicationCharacteristics = "article_or_review"
                }
                else {
                    recordObject.publicationCharacteristics = "other"
                }
                break;
            case 'Typ formalny publikacji':
                // ? W nowej wersji pliku nie ma tych wartości ?
                // Teraz jest np. Charakt. formalna: fragment/rozdział ze zbiorówki
                let publicationType = property.value;
                if (publicationType === '008') pageCountTowardsSumConditions.typFormalny = true;
                if (publicationType === '002') recordObject.ministerialArticle = true;
                break;
            case 'Typ merytoryczny publikacji':
                // ? W nowej wersji pliku nie ma tych wartości ?
                // Teraz jest np. Charakt. merytoryczna: referat w materiałach z konferencji, zjazdu zagraniczny
                if (['KNP', 'PAP', 'PSP'].indexOf(property.value.substr(0, 3)) !== -1) {
                    recordObject.publicationType = 'book';
                    pageCountTowardsSumConditions.typMerytoryczny = true;
                } else if ('ECP' === property.value.substr(0, 3) || 'EZ' === property.value.substr(0, 2)) {
                    recordObject.publicationType = 'edit'
                } else recordObject.publicationType = 'article';
                break;
            case 'Język publikacji':
                recordObject.languages.push(property.value);
                break;
            case 'Słowa kluczowe':
                property.value.split(';').forEach((keyword) => {
                    recordObject.polishkeywords.push(keyword.trim());
                });
                break;
            case 'Słowa kluczowe angielskie':
                property.value.split(';').forEach((keyword) => {
                    recordObject.englishkeywords.push(keyword.trim());
                });
                break;
            case 'Opis fizyczny':
                const pageRange = property.value.match(/[0-9]{1,5}-[0-9]{1,5}/);
                if (pageRange != null) {
                    const pageArray = pageRange[0].split('-');
                    recordObject.pageRange = pageRange[0];
                    recordObject.pageAmount = Number.parseInt(pageArray[1]) - Number.parseInt(pageArray[0]) + 1;
                    pageCountTowardsSumConditions.czyWycinek = true;
                } else if (property.value.match(/[0-9]{1,5}/) != null) {
                    recordObject.pageRange = '0-' + property.value.match(/[0-9]{1,5}/)[0].trim();
                    recordObject.pageAmount = Number.parseInt(property.value.match(/[0-9]{1,5}/)[0].trim());
                } else {
                    logger.logError(new Error('Failed RegExp parse at pageRange of line: ' + property.value).message);
                    recordObject.invalid["300"] = 'Could not infer page range from description';
                    recordObject.unparsedPageAmount = property.value.substr(4);
                }
                break;
            case 'Punktacja MNiSW':
                recordObject.points = Number.parseFloat(property.value);
                break;
            case 'Impact Factor':
                recordObject.impactFactor = Number.parseFloat(property.value)
                break;
            case 'Konferencja/zjazd - tytuł':
                recordObject.conferencePaper = true;
                break;
            case 'Konferencja/zjazd - miejsce i data':
                recordObject.conferencePaper = true;
                break;
            case 'open-access-text-version':
                recordObject.openAccessArticle = true;
                break;
            case 'open-access-licence':
                recordObject.openAccessArticle = true;
                break;
            case 'open-access-release-time':
                recordObject.openAccessArticle = true;
                break;
            case 'open-access-article-mode':
                recordObject.openAccessArticle = true;
                break;
            case 'Czasopismo':
                recordObject.journalTitle = property.value
                recordObject.compJournalTitle = recordObject.journalTitle.toLowerCase();
                [' ', '.', ',', '\'', '', ':', ';'].forEach(function (ignoredChar) {
                    recordObject.compJournalTitle = recordObject.compJournalTitle.split(ignoredChar).join('')
                })
                if (journals[recordObject.compJournalTitle]) {
                    let city = journals[recordObject.compJournalTitle].city;
                    if (city) {
                        recordObject.city = city;
                        let latLng = cities[city];
                        recordObject.latLng = latLng;
                        if (latLng !== null && latLng !== undefined)
                            recordObject.country = latLng.country;
                    }
                }
                break;
            default:
                break;
        }
    })
    let countPages = (
            (recordObject.authors.length === 1 && (pageCountTowardsSumConditions.typFormalny || pageCountTowardsSumConditions.typMerytoryczny))
            || pageCountTowardsSumConditions.czyWycinek);
    if (!countPages) recordObject.pageAmount = 0;

    return recordObject;
}

parser.parseRtf = function (fileName, rawText, done) {
    try{
        parseRTF.string(rawText, (err, doc) => {
            try {
                let queryName = fileName
                // divide doc into array of raw records
                let rawRecords = splitRtfDocToRawRecordsArray(doc);

                // for each raw record create record object
                let recordObjectsArray = []
                rawRecords.forEach((rawRecord) => {
                    let recordObject = createRecordObjectFromRawRecord(rawRecord)
                    recordObjectsArray.push(recordObject)
                })
                //return array with record objects
                done(null, recordObjectsArray, queryName);
            } catch (err) {
                done(err, null, null)
            }
        })
    } catch (e) {
        done(e, null, null)
    }
}

parser.parseCsv = function (rawText, done) {
    try {
        let delimiter = ","
        const headers = rawText.slice(0, rawText.indexOf("\n")).split(delimiter);
        const rows = rawText.slice(rawText.indexOf("\n") + 1).split("\n");
        let recordObjectsArray = [];
        let queryName = "TEST CSV"
        done(null, recordObjectsArray, queryName);
    } catch (err) {
        done(err, null, null)
    }

}

module.exports = parser;
