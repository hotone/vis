const d3 = require("d3");
const {Disciplines} = require("../sources/js/disciplines");
const logger = require("./logger");

function computeSumOfWorks(worksList) {
    if (worksList !== null){
        return worksList.length
    } else {
        return 0;
    }
}

function computeSumOfPoints(worksList) {
    let sum = 0
    worksList.forEach((work) => {
        sum += Number(work.points)
    })
    return sum
}

function computeAveragePoints(worksList) {
    let worksPerYear = {}
    let uniqueYears = 0
    worksList.forEach((work) => {
        if (!(worksPerYear.hasOwnProperty(String(work.year)))) {
            worksPerYear[String(work.year)] = {
                "sumOfPoints": Number(work.points),
                "sumOfWorksInYear": 1
            }
            uniqueYears += 1
        } else {
            worksPerYear[String(work.year)]["sumOfPoints"] += Number(work.points)
            worksPerYear[String(work.year)]["sumOfWorksInYear"] += 1
        }
    })
    let sumOfAllPoints = computeSumOfPoints(worksList)
    let average = Number(sumOfAllPoints) / Number(uniqueYears)
    return average.toFixed(2)
}

function computeCoauthorsAmount(worksList) {
    let coauthors = {}
    let coauthorsAmount = 0
    worksList.forEach((work) => {
        work.authors.forEach((author) => {
            if (!(coauthors.hasOwnProperty(String(author)))) {
                coauthors[String(author)] = 1
                coauthorsAmount += 1
            } else {
                coauthors[String(author)] += 1
            }
        })
    })
    return coauthorsAmount
}

function findMainCoauthor(worksList, authorName) {
    let coauthors = {}
    worksList.forEach((work) => {
        work.authors.forEach((author) => {
            if (!(coauthors.hasOwnProperty(String(author)))) {
                coauthors[String(author)] = 1
            } else {
                coauthors[String(author)] += 1
            }
        })
    })
    let mainCoauthor = ""
    // author with the biggest number is actual author for whom we are looking main coauthor
    let actualAuthor = Object.keys(coauthors).reduce(
        (oneAuthor, comparedAuthor) => coauthors[oneAuthor] > coauthors[comparedAuthor] ? oneAuthor : comparedAuthor)
    mainCoauthor = Object.keys(coauthors).reduce(
        (oneAuthor, comparedAuthor) => coauthors[oneAuthor] > coauthors[comparedAuthor] && oneAuthor !== actualAuthor ? oneAuthor : comparedAuthor)
    return mainCoauthor
}

function findAuthorName(worksList) {
    let coauthors = {}
    worksList.forEach((work) => {
        work.authors.forEach((author) => {
            if (!(coauthors.hasOwnProperty(String(author)))) {
                coauthors[String(author)] = 1
            } else {
                coauthors[String(author)] += 1
            }
        })
    })
    // author with the biggest number is actual author for whom we are looking main coauthor
    let actualAuthor = Object.keys(coauthors).reduce(
        (oneAuthor, comparedAuthor) => coauthors[oneAuthor] > coauthors[comparedAuthor] ? oneAuthor : comparedAuthor)
    return actualAuthor
}

function computeSummedImpactFactor(worksList) {
    let summedImpactFactor = 0;
    worksList.forEach(work => {
        summedImpactFactor += work.impactFactor;
    })
    return summedImpactFactor.toFixed(2);
}

function getConferencePapersAmount(worksList) {
    let summedConferencePapersAmount = 0;
    worksList.forEach(work => {
        if(work.conferencePaper)
            summedConferencePapersAmount++;
    })
    return summedConferencePapersAmount;
}

function getOpenAccessPapersAmount(worksList) {
    let summedOpenAccessPapersAmount = 0;
    worksList.forEach(work => {
        if(work.openAccessArticle)
            summedOpenAccessPapersAmount++;
    })
    return summedOpenAccessPapersAmount;
}

function computeEngToPlPapersRatio(worksList) {
    let polishPapersAmount = 0;
    let englishPapersAmount = 0;
    worksList.forEach(work => {
        work.languages.forEach(lang => {
            if (lang.trim().toLowerCase() === "eng"){
                englishPapersAmount += 1
            } else if (lang.trim().toLowerCase() === "pol"){
                polishPapersAmount += 1
            }
        })
    })
    if (polishPapersAmount !== 0) {
        return (englishPapersAmount / (englishPapersAmount + polishPapersAmount)).toFixed(2);
    }
    return 1;
}

function getPublishingBegginingYear(worksList) {
    let allYears = [];
    worksList.forEach(work => {
        if (!allYears.includes(work.year)){
            allYears.push(work.year)
        }
    })
    return Math.min(...allYears);
}

function computeMultidisciplinaryAreaOfScientificInterests(works) {
    let articlePointsOnGraph = []

    Object(works).forEach((work) => {
        if (work.disciplines !== null) {
            if (work.disciplines.length === 0) {
                return;
            } else {
                let articlePointOnGraph = {
                    disciplinesList: []
                }

                Object(work.disciplines).forEach((discipline) => {
                    let disciplineObj = null
                    let disciplineName = null

                    if (discipline.startsWith(" ") || discipline.endsWith(" ")) {
                        disciplineName = discipline.trim()
                        disciplineObj = Disciplines[disciplineName]
                    } else {
                        disciplineName = discipline
                        disciplineObj = Disciplines[disciplineName]
                    }

                    articlePointOnGraph.disciplinesList.push(
                        {
                            x: parseInt(disciplineObj["coordinates"]["X"]),
                            y: parseInt(disciplineObj["coordinates"]["Y"]),
                        }
                    )
                })
                articlePointsOnGraph.push(articlePointOnGraph)
            }
        }
    })

    Object(articlePointsOnGraph).forEach((article) => {

        let listOfCoordinates = []

        Object(article.disciplinesList).forEach((discipline) => {
            listOfCoordinates.push([discipline.x, discipline.y])
        })

        let hullCoordinates = d3.polygonHull(listOfCoordinates)

        let pointCoordinates
        if (listOfCoordinates !== null){
            if (listOfCoordinates.length > 2){
                pointCoordinates = d3.polygonCentroid(hullCoordinates)
                // if not calculating hull first
                // pointCoordinates = d3.polygonCentroid(listOfCoordinates)
            } else if (listOfCoordinates.length === 2){
                pointCoordinates = [
                    Math.floor((listOfCoordinates[0][0] + listOfCoordinates[1][0]) / 2),
                    Math.floor((listOfCoordinates[0][1] + listOfCoordinates[1][1]) / 2)
                ]
            } else {
                pointCoordinates = listOfCoordinates[0]
            }

            article.x = pointCoordinates[0]
            article.y = pointCoordinates[1]
            article.coordinates = pointCoordinates
        }

    })

    let disciplinesPoints = []

    for (let discipline of Object.keys(Disciplines)) {
        let entry = Disciplines[discipline.trim()]
        disciplinesPoints.push([
            Math.floor(parseInt(entry["coordinates"]["X"])),
            Math.floor(parseInt(entry["coordinates"]["Y"]))
        ])
    }

    let points = []

    for (const article of articlePointsOnGraph){
        points.push([Math.floor(article.x), Math.floor(article.y)])
    }


    let maxPossiblePolygonHull = d3.polygonHull(disciplinesPoints)
    const spaceArea = d3.polygonArea(maxPossiblePolygonHull)
    if (points.length > 2){
        let polygonHull = d3.polygonHull(points)
        let polygonArea = d3.polygonArea(polygonHull)
        return (polygonArea / spaceArea).toFixed(2)
    } else{
        return 0;
    }

}

function getCoreCollaboration10(worksList) {
    let coauthors = {}
    worksList.forEach((work) => {
        work.authors.forEach((author) => {
            if (!(coauthors.hasOwnProperty(String(author)))) {
                coauthors[String(author)] = 1
            } else {
                coauthors[String(author)] += 1
            }
        })
    })
    if (Object.keys(coauthors) !== null){
        if (Object.keys(coauthors).length < 10) {
            return 0;
        } else {
            let sorted = [];
            for (let coauthor in coauthors) {
                sorted.push([coauthor, coauthors[coauthor]]);
            }
            sorted.sort(function(a, b) {
                return  b[1] - a[1];
            });

            let coreCollaboration10WorksSum = 0;
            for(let index=1; index<=10; index++){
                if (sorted[index] !== undefined)
                    coreCollaboration10WorksSum += sorted[index][1]
            }
            return (coreCollaboration10WorksSum / computeSumOfWorks(worksList)).toFixed(2);
        }
    }
}

function getscatteringFactor(worksList) {
    let coauthors = {}
    worksList.forEach((work) => {
        work.authors.forEach((author) => {
            if (!(coauthors.hasOwnProperty(String(author)))) {
                coauthors[String(author)] = 1
            } else {
                coauthors[String(author)] += 1
            }
        })
    })

    let sorted = [];
    for (let coauthor in coauthors) {
        sorted.push([coauthor, coauthors[coauthor]]);
    }
    sorted.sort(function(a, b) {
        return  b[1] - a[1];
    });

    let sumOfWorksWithMostExternalRingCoauthors = 0;

    sorted.forEach((entry) => {
        if(entry[1] === 1){
            sumOfWorksWithMostExternalRingCoauthors += 1;
        }
    })
    return (sumOfWorksWithMostExternalRingCoauthors / computeSumOfWorks(worksList)).toFixed(2);
}

function countPapersByNationality(worksList) {
    let allPapersAmount = 0;
    let nationalPapersAmount = 0;
    let interNationalPapersAmount = 0;

    worksList.forEach(work => {
        if (work.country !== null) {
            if (work.country === "PL")
                nationalPapersAmount++;
            else
                interNationalPapersAmount++;
        }
        allPapersAmount++;
    })
    return {allPapersAmount, nationalPapersAmount, interNationalPapersAmount};
}

function percentageOfComputedWorksForNationalityOfPapers(worksList) {
    let {allPapersAmount, nationalPapersAmount, interNationalPapersAmount} = countPapersByNationality(worksList);
    return ((nationalPapersAmount + interNationalPapersAmount) / allPapersAmount).toFixed(2);
}

function getrangeOfNationalCooperation(worksList) {
    let {nationalPapersAmount, interNationalPapersAmount} = countPapersByNationality(worksList);
    return (nationalPapersAmount / (nationalPapersAmount + interNationalPapersAmount)).toFixed(2);
}

function getRangeOfInternationalCooperation(worksList) {
    let {nationalPapersAmount, interNationalPapersAmount} = countPapersByNationality(worksList);
    return (interNationalPapersAmount / nationalPapersAmount).toFixed(2);
}

function percentageOfComputedWorksForMasi(worksList){
    let amountOfComputedWorksForMasi = 0;
    let amountOfAllWorks = 0;
    Object(worksList).forEach((work) => {
        if (work.disciplines !== null)
            if (work.disciplines.length !== 0) {
                amountOfComputedWorksForMasi ++;
            }
        amountOfAllWorks++;
    });
    return (amountOfComputedWorksForMasi / amountOfAllWorks).toFixed(2);
}

function computeStatistics(worksList, authorName) {
    return {
        sumOfWorks: computeSumOfWorks(worksList),
        sumOfPoints: computeSumOfPoints(worksList),
        pointsAverage: computeAveragePoints(worksList),
        coauthorsAmount: computeCoauthorsAmount(worksList),
        mainCoauthor: findMainCoauthor(worksList, authorName),
        authorName: findAuthorName(worksList),
        impactFactor: computeSummedImpactFactor(worksList),
        conferencePapersAmount: getConferencePapersAmount(worksList),
        openAccessPapersAmount: getOpenAccessPapersAmount(worksList),
        openAccessPapersRatio: (getOpenAccessPapersAmount(worksList)/computeSumOfWorks(worksList)).toFixed(2),
        conferencePapersRatio: (getConferencePapersAmount(worksList)/computeSumOfWorks(worksList)).toFixed(2),
        engToPlPapersRatio: computeEngToPlPapersRatio(worksList),
        publishingBegginingYear: getPublishingBegginingYear(worksList),
        rangeOfNationalCooperation: getrangeOfNationalCooperation(worksList),
        rangeOfInternationalCooperation: getRangeOfInternationalCooperation(worksList),
        percentageOfComputedWorksForNationalityOfPapers: percentageOfComputedWorksForNationalityOfPapers(worksList),
        coreCollaboration10: getCoreCollaboration10(worksList),
        scatteringFactor: getscatteringFactor(worksList),
        multidisciplinaryAreaOfScientificInterests: computeMultidisciplinaryAreaOfScientificInterests(worksList),
        percentageOfComputedWorksForMasi: percentageOfComputedWorksForMasi(worksList),
    }
}

module.exports = computeStatistics
