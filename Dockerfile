FROM node:10
RUN apt-get update \
    && apt-get install -y \
        libjpeg-dev \
        build-essential \
        libcairo2-dev \
        libpango1.0-dev \
        libgif-dev \
        librsvg2-dev
EXPOSE 3000
WORKDIR /app
COPY ["package.json", "package-lock.json*", "./"]
RUN npm install
COPY . .
RUN npm run build
ENTRYPOINT ["npm", "run", "prod"]
