const path = require('path');

module.exports = {
  mode: 'development',
  devtool: 'inline-source-map',
  entry: {
    'bubbles': './sources/js/visualisations/bubbles.js',
    'collab': './sources/js/visualisations/collab.js',
    'google-map': './sources/js/visualisations/google-map.js',
    'research-map': './sources/js/visualisations/research-map.js',
    'wordcloud': './sources/js/visualisations/wordcloud.js',
    'works': './sources/js/visualisations/works.ts',
  },
  devServer: {
    static: './dist',
    hot: true
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/
      }
    ]
  },
  resolve: {
    extensions: [ '.tsx', '.ts', '.js' ]
  },
  output: {
    path: path.resolve(__dirname, 'dist')
  }
};
