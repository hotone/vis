import 'cypress-file-upload';

Cypress.Commands.add('login', (user, password) => {
    cy.contains("Nazwa użytkownika").type(user)
    cy.contains("Hasło").type(password)
    cy.get('.login-submit > .btn').click()
})
