export function checkChosenStatisticValue(statisticName, expectedValue) {
    expect(cy.contains(statisticName).should('be.visible'))
    cy.contains(statisticName).next("span").then((element) => {
        expect(element.text()).to.be.equal(expectedValue)
    })
}


