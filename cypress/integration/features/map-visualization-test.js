Cypress.on('uncaught:exception', (err, runnable) => {
    return false
});

describe('Map visualization tests', function () {

    beforeEach(function () {
        cy.visit(Cypress.env('url'))
        cy.fixture('test_data.json').then((data) => {
            cy.login(data.user, data.password)
        })

    })

    afterEach(function (){
        cy.visit(`${Cypress.env('url')}/logout`)
    })

    it('Before loading any file with data map visualization should be disabled', function (){
        cy.wait(2000)
        cy.get('img[src="/images/google-map.jpg"]').parent('div').should('have.class', 'disabled')
    })

    it('Loads file with example data and map visualization should be now enabled', function (){
        cy.get('input[type=file]').attachFile({ filePath: 'veslava_osinska.rtf', encoding: 'base64' })
        cy.wait(3000)
        cy.get('img[src="/images/google-map.jpg"]').parent('div').should('not.have.class', 'disabled')
    })

})
