describe('Login to app feature tests', function () {

    before(function (){
        cy.fixture('test_data.json').then((data) => {
            this.testData = data
        })
    })

    beforeEach(function (){
        cy.visit(Cypress.env('url'))
    })

    it('Login to app properly', function () {
        cy.contains("Nazwa użytkownika").type(this.testData.user)
        cy.contains("Hasło").type(this.testData.password)
        cy.get('.login-submit > .btn').click()
        expect(cy.contains("Informacje o wizualizacji"))
    })

    it('Do not let login with wrong credentials', function (){
        cy.contains("Nazwa użytkownika").type("NON-EXISTING-USER")
        cy.contains("Hasło").type("BAD-PASSWORD")
        cy.get('.login-submit > .btn').click()
        expect(cy.contains("Nazwa użytkownika"))
        expect(cy.contains("Hasło"))
        expect(cy.get('.login-submit > .btn').should('be.visible'))
    })

    it('Do not let login without any credentials', function (){
        cy.get('.login-submit > .btn').click()
        expect(cy.contains("Nazwa użytkownika"))
        expect(cy.contains("Hasło"))
        expect(cy.get('.login-submit > .btn').should('be.visible'))
    })
})
