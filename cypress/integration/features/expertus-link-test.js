describe('Expertus link test', function () {

    before(function () {
        cy.fixture('test_data.json').then((data) => {
            this.testData = data
        })
    })

    beforeEach(function () {
        cy.visit(Cypress.env('url'))
        cy.login(this.testData.user, this.testData.password)
    })

    it('After login to main page should have proper link to Expertus base', function (){
        cy.contains('Baza Expertus').click()
        expect(cy.url().should('include', "https://bg.cm.umk.pl/expertus/umk/"))
    })
})
