import {checkChosenStatisticValue} from "../../support/helpers";

Cypress.on('uncaught:exception', (err, runnable) => {
    return false
});

describe('Displaying statistics feature tests', function () {

    beforeEach(function () {
        cy.visit(Cypress.env('url'))
        cy.fixture('test_data.json').then((data) => {
            cy.login(data.user, data.password)
        })

    })

    afterEach(function (){
        cy.visit(`${Cypress.env('url')}/logout`)
    })

    it('Right after login displayed statistics should be blank', function () {
        expect(cy.contains("Informacje o wizualizacji").should('be.visible'))
        expect(cy.contains("Statystyki").should('be.visible'))

        checkChosenStatisticValue("Łącznie prac", "-")
        checkChosenStatisticValue("Łączna punktacja MNiSW", "-")
        checkChosenStatisticValue("Średnia liczba punktów MNiSW na rok", "-")
        checkChosenStatisticValue("Liczba współautorów", "-")
        checkChosenStatisticValue("Najczęstsza współpraca", "-")
    })

    it('Loads file with example data and displayed statistics should be changed', function (){
        expect(cy.contains("Informacje o wizualizacji").should('be.visible'))
        expect(cy.contains("Statystyki").should('be.visible'))

        cy.get('input[type=file]').attachFile({ filePath: 'veslava_osinska_new.rtf', encoding: 'base64' })
        cy.wait(3000)

        checkChosenStatisticValue("Łącznie prac", "57")
        checkChosenStatisticValue("Łączna punktacja MNiSW", "273")
        checkChosenStatisticValue("Średnia liczba punktów MNiSW na rok", "16.06")
        checkChosenStatisticValue("Liczba współautorów", "26")
        checkChosenStatisticValue("Najczęstsza współpraca", "Osiński Grzegorz")
    })

    it('After loading file it resets results and displayed statistics should be reset', function (){
        expect(cy.contains("Informacje o wizualizacji").should('be.visible'))
        expect(cy.contains("Statystyki").should('be.visible'))

        cy.get('input[type=file]').attachFile({ filePath: 'veslava_osinska.rtf', encoding: 'base64' })
        cy.wait(3000)

        checkChosenStatisticValue("Łącznie prac", "57")
        checkChosenStatisticValue("Łączna punktacja MNiSW", "273")
        checkChosenStatisticValue("Średnia liczba punktów MNiSW na rok", "16.06")
        checkChosenStatisticValue("Liczba współautorów", "26")
        checkChosenStatisticValue("Najczęstsza współpraca", "Osiński Grzegorz")

        cy.contains("Resetuj wyniki").click()
        cy.wait(3000)

        checkChosenStatisticValue("Łącznie prac", "-")
        checkChosenStatisticValue("Łączna punktacja MNiSW", "-")
        checkChosenStatisticValue("Średnia liczba punktów MNiSW na rok", "-")
        checkChosenStatisticValue("Liczba współautorów", "-")
        checkChosenStatisticValue("Najczęstsza współpraca", "-")
    })
})
