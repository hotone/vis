const Disciplines = {
    "astronomia": {
        "amount_of_journals": "591",
        "coordinates": {
            "X": "609",
            "Y": "10"
        },
        "scientific_field": "ścisłe-przyrodncze"
    },
    "architektura i urbanistyka": {
        "amount_of_journals": "993",
        "coordinates": {
            "X": "708",
            "Y": "130"
        },
        "scientific_field": "techniczne"
    },
    "sztuki filmowe i teatralne": {
        "amount_of_journals": "431",
        "coordinates": {
            "X": "538",
            "Y": "793"
        },
        "scientific_field": "sztuka"
    },
    "sztuki muzyczne": {
        "amount_of_journals": "233",
        "coordinates": {
            "X": "467",
            "Y": "799"
        },
        "scientific_field": "sztuka"
    },
    "sztuki plastyczne i konserwacja dzieł sztuki": {
        "amount_of_journals": "901",
        "coordinates": {
            "X": "426",
            "Y": "822"
        },
        "scientific_field": "sztuka"
    },
    "nauki o sztuce":{
        "amount_of_journals": "1565",
        "coordinates": {
            "X": "477",
            "Y": "804"
        },
        "scientific_field": "sztuka"
    },
    "matematyka": {
        "amount_of_journals": "1607",
        "coordinates": {
            "X": "793",
            "Y": "255"
        },
        "scientific_field": "ścisłe-przyrodncze"
    },
    "nauki prawne": {
        "amount_of_journals": "1733",
        "coordinates": {
            "X": "708",
            "Y": "595"
        },
        "scientific_field": "społeczne"
    },
    "nauki teologiczne": {
        "amount_of_journals": "1908",
        "coordinates": {
            "X": "382",
            "Y": "779"
        },
        "scientific_field": "teologia"
    },
    "nauki fizyczne": {
        "amount_of_journals": "2135",
        "coordinates": {
            "X": "425",
            "Y": "28"
        },
        "scientific_field": "ścisłe-przyrodncze"
    },
    "językoznawstwo": {
        "amount_of_journals": "2175",
        "coordinates": {
            "X": "590",
            "Y": "765"
        },
        "scientific_field": "humanistyczne"
    },
    "automatyka, elektronika i elektrotechnika": {
        "amount_of_journals": "2206",
        "coordinates": {
            "X": "595",
            "Y": "155"
        },
        "scientific_field": "techniczne"
    },
    "inżynieria lądowa i transport": {
        "amount_of_journals": "2328",
        "coordinates": {
            "X": "538",
            "Y": "99"
        },
        "scientific_field": "techniczne"
    },
    "literaturoznawstwo": {
        "amount_of_journals": "2354",
        "coordinates": {
            "X": "652",
            "Y": "722"
        },
        "scientific_field": "humanistyczne"
    },
    "pedagogika": {
        "amount_of_journals": "2494",
        "coordinates": {
            "X": "396",
            "Y": "596"
        },
        "scientific_field": "społeczne"
    },
    "informatyka": {
        "amount_of_journals": "2510",
        "coordinates": {
            "X": "666",
            "Y": "297"
        },
        "scientific_field": "ścisłe-przyrodncze"
    },
    "archeologia": {
        "amount_of_journals": "2544",
        "coordinates": {
            "X": "368",
            "Y": "666"
        },
        "scientific_field": "humanistyczne"
    },
    "nauki o komunikacji społecznej i mediach": {
        "amount_of_journals": "2649",
        "coordinates": {
            "X": "637",
            "Y": "609"
        },
        "scientific_field": "społeczne"
    },
    "ekonomia i finanse": {
        "amount_of_journals": "2669",
        "coordinates": {
            "X": "652",
            "Y": "510"
        },
        "scientific_field": "społeczne"
    },
    "historia": {
        "amount_of_journals": "2710",
        "coordinates": {
            "X": "524",
            "Y": "708"
        },
        "scientific_field": "humanistyczne"
    },
    "inżynieria materiałowa": {
        "amount_of_journals": "2721",
        "coordinates": {
            "X": "439",
            "Y": "113"
        },
        "scientific_field": "techniczne"
    },
    "nauki chemiczne": {
        "amount_of_journals": "2898",
        "coordinates": {
            "X": "297",
            "Y": "141"
        },
        "scientific_field": "ścisłe-przyrodncze"
    },
    "nauki o kulturze i religii": {
        "amount_of_journals": "2929",
        "coordinates": {
            "X": "467",
            "Y": "666"
        },
        "scientific_field": "humanistyczne"
    },
    "prawo kanoniczne": {
        "amount_of_journals": "2934",
        "coordinates": {
            "X": "566",
            "Y": "666"
        },
        "scientific_field": "społeczne"
    },
    "nauki o Ziemi i środowisku": {
        "amount_of_journals": "3074",
        "coordinates": {
            "X": "425",
            "Y": "320"
        },
        "scientific_field": "ścisłe-przyrodncze"
    },
    "inżynieria chemiczna": {
        "amount_of_journals": "3133",
        "coordinates": {
            "X": "396",
            "Y": "141"
        },
        "scientific_field": "techniczne"
    },
    "nauki o zarządzaniu i jakości": {
        "amount_of_journals": "3141",
        "coordinates": {
            "X": "586",
            "Y": "453"
        },
        "scientific_field": "społeczne"
    },
    "filozofia": {
        "amount_of_journals": "3239",
        "coordinates": {
            "X": "453",
            "Y": "708"
        },
        "scientific_field": "humanistyczne"
    },
    "inżynieria mechaniczna": {
        "amount_of_journals": "3467",
        "coordinates": {
            "X": "490",
            "Y": "194"
        },
        "scientific_field": "techniczne"
    },
    "technologia żywności i żywienia": {
        "amount_of_journals": "3500",
        "coordinates": {
            "X": "226",
            "Y": "198"
        },
        "scientific_field": "rolnicze"
    },
    "informatyka techniczna i telekomunikacja": {
        "amount_of_journals": "3566",
        "coordinates": {
            "X": "589",
            "Y": "261"
        },
        "scientific_field": "techniczne"
    },
    "nauki o polityce i administracji": {
        "amount_of_journals": "3602",
        "coordinates": {
            "X": "538",
            "Y": "609"
        },
        "scientific_field": "społeczne"
    },
    "weterynaria": {
        "amount_of_journals": "3768",
        "coordinates": {
            "X": "170",
            "Y": "283"
        },
        "scientific_field": "rolnicze"
    },
    "geografia społeczno-ekonomiczna i gospodarka przestrzenna": {
        "amount_of_journals": "3817",
        "coordinates": {
            "X": "453",
            "Y": "439"
        },
        "scientific_field": "społeczne"
    },
    "nauki o bezpieczeństwie": {
        "amount_of_journals": "4134",
        "coordinates": {
            "X": "552",
            "Y": "538"
        },
        "scientific_field": "społeczne"
    },
    "nauki leśne": {
        "amount_of_journals": "4280",
        "coordinates": {
            "X": "240",
            "Y": "270"
        },
        "scientific_field": "rolnicze"
    },
    "rolnictwo i ogrodnictwo": {
        "amount_of_journals": "4710",
        "coordinates": {
            "X": "297",
            "Y": "255"
        },
        "scientific_field": "rolnicze"
    },
    "psychologia": {
        "amount_of_journals": "4868",
        "coordinates": {
            "X": "350",
            "Y": "496"
        },
        "scientific_field": "społeczne"
    },
    "inżynieria środowiska, górnictwo i energetyka": {
        "amount_of_journals": "4903",
        "coordinates": {
            "X": "411",
            "Y": "240"
        },
        "scientific_field": "techniczne"
    },
    "zootechnika i rybactwo": {
        "amount_of_journals": "4997",
        "coordinates": {
            "X": "212",
            "Y": "340"
        },
        "scientific_field": "rolnicze"
    },
    "nauki socjologiczne": {
        "amount_of_journals": "5089",
        "coordinates": {
            "X": "467",
            "Y": "567"
        },
        "scientific_field": "społeczne"
    },
    "nauki o kulturze fizycznej": {
        "amount_of_journals": "5251",
        "coordinates": {
            "X": "198",
            "Y": "425"
        },
        "scientific_field": "medyczne"
    },
    "nauki biologiczne": {
        "amount_of_journals": "5480",
        "coordinates": {
            "X": "269",
            "Y": "311"
        },
        "scientific_field": "ścisłe-przyrodncze"
    },
    "nauki farmaceutyczne": {
        "amount_of_journals": "6373",
        "coordinates": {
            "X": "283",
            "Y": "354"
        },
        "scientific_field": "medyczne"
    },
    "inżynieria biomedyczna": {
        "amount_of_journals": "7275",
        "coordinates": {
            "X": "354",
            "Y": "297"
        },
        "scientific_field": "techniczne"
    },
    "nauki o zdrowiu": {
        "amount_of_journals": "8758",
        "coordinates": {
            "X": "270",
            "Y": "425"
        },
        "scientific_field": "medyczne"
    },
    "nauki medyczne": {
        "amount_of_journals": "8874",
        "coordinates": {
            "X": "250",
            "Y": "375"
        },
        "scientific_field": "medyczne"
    }
}

module.exports = {Disciplines}
