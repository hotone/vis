import * as d3 from 'd3';
import {InjectContext} from "../common";

document.addEventListener('DOMContentLoaded', () => InjectContext( (works, strings) => {
	let removedCounter  = 0;
	works.forEach(function (invalidEl, invalidIndex) {
		if(!invalidEl.year || invalidEl.pageAmount < 0) {
			works.splice(invalidIndex + removedCounter, 1)}
	});

	works = works.sort( (a, b) => {
		if(a.year && b.year) return (a.year > b.year) - (a.year < b.year);
	});

	const PageCountsGroupedByYear = [];
	PageCountsGroupedByYear.push({
		year: works[0].year,
		pages: 0,
		points: 0
	});

	works.forEach(function (el) {
		if(el.pageAmount < 0) el.pageAmount = 0;
		const last = PageCountsGroupedByYear.length - 1;
		if(PageCountsGroupedByYear[last].year === el.year) {
			PageCountsGroupedByYear[last].pages += el.pageAmount;
			PageCountsGroupedByYear[last].points += el.points;
		}
		else PageCountsGroupedByYear.push({
			year: el.year,
			pages: el.pageAmount,
			points: el.points
		})
	});

	const $buttonHost = document.querySelector('#genPdfBtn').parentElement;

	const slider = document.createElement('input');
	slider.setAttribute('type', 'range');
	slider.setAttribute('style', 'border: none');
	slider.setAttribute('min', '0.1');
	slider.setAttribute('max', '1.0');
	slider.setAttribute('step', '0.1');
	slider.setAttribute('id', 'keywords-slider');
	slider.value = '0.1';
	slider.onchange = () => {
		sliderDisplay.innerText = `${strings.bubbles.scale_factor}: ${slider.value}`
		d3.selectAll('#svg-port > *').remove();
		drawGraph(works, strings, PageCountsGroupedByYear, slider.value);
	}

	const sliderDisplay = document.createElement('p');
	sliderDisplay.setAttribute('id', 'slider-display');
	sliderDisplay.innerText = `${strings.bubbles.scale_factor}: ${slider.value}`

	const sliderDiv = document.createElement('div');
	sliderDiv.setAttribute('class', 'row');
	sliderDiv.appendChild(sliderDisplay);
	sliderDiv.appendChild(slider);

	$buttonHost.appendChild(sliderDiv);


	drawGraph(works, strings, PageCountsGroupedByYear);
}));

function range(start, end) {
	return Array.from({ length: end - start + 1 }, (_, i) => start + i)
}

class ColorGenerator{
	constructor() {
		this.colorCounter = 0;
	}

	NextColor() {
		if (this.colorCounter >= 9) this.colorCounter = 0;
		return d3.schemeSet2[this.colorCounter++];
	}
}

function drawGraph(works, translationStrings, pageCountsByYear, bubbleScaleFactor=0.1){
	const margin = { top: 50, right: 50, bottom: 50, left: 50 }

	const svg = d3.select('#svg-port');

	const width = parseInt(svg.style('width').replace('px', ''));
	const height = parseInt(svg.style('height').replace('px', ''));
	const innerWidth = width - margin.left - margin.right;
	const innerHeight = height - margin.top - margin.bottom;

	svg.append('svg')
		.attr("width", width)
		.attr("height", height)
		.append("g")
		.attr("transform", `translate(${margin.left},${margin.top})`);

	let yearsRange = getYearRange(works);
	let pointsRange = getScoreRange(works);
	const types = range(yearsRange[0], yearsRange[1]);

	const x = d3.scaleBand()
		.domain(types)
		.range([0, innerWidth]);

	let yMax = pointsRange[1] + 0.3 * pointsRange[1];
	const y = d3.scaleLinear()
		.domain([0, yMax])
		.range([innerHeight, 0]);

	const xAxis = d3.axisBottom(x).ticks(types.length);
	const yAxis = d3.axisLeft(y).ticks(10);
	const xAxisGrid = d3.axisBottom(x).tickSize(-innerHeight).tickFormat('').ticks(types.length);
	const yAxisGrid = d3.axisLeft(y).tickSize(-innerWidth).tickFormat('').ticks(10);

	// Create grids.
	svg.append('g')
		.attr('class', 'x axis-grid')
		.attr("transform", `translate(${margin.left},${innerHeight + margin.top})`)
		.attr("stroke", "lightblue")
		.call(xAxisGrid)

	svg.append('g')
		.attr('class', 'y axis-grid')
		.attr("transform", `translate(${margin.left},${margin.top})`)
		.attr("stroke", "lightblue")
		.call(yAxisGrid);
	// Create axes.
	svg.append('g')
		.attr('class', 'x axis')
		.attr("transform", `translate(${margin.left},${innerHeight + margin.top})`)
		.attr("stroke", "black")
		.call(xAxis)
		.append("text")
		// .attr("fill", "black")
		.attr("transform", `translate(${innerWidth/2}, 40)`)
		.text(`${translationStrings.bubbles.x_axis_description}`);

	svg.append('g')
		.attr('class', 'y axis')
		.attr("transform", `translate(${margin.left},${margin.top})`)
		.attr("stroke", "black")
		.call(yAxis)
		.append("text")
		// .attr("fill", "black")
		.attr("transform", `translate(-30, ${innerHeight/2 - margin.top}) rotate(-90)`)
		.text(`${translationStrings.bubbles.y_axis_description}`);

	let colorGenerator = new ColorGenerator();
	svg.append("g")
		.selectAll("bubble")
		.data(pageCountsByYear)
		.enter()
		.append("circle")
		.attr("cx", data => x(data.year) + margin.left)
		.attr("cy", data => y(data.points) + margin.top)
		.attr("r", data => data.pages * bubbleScaleFactor)
		.attr('fill', function () {
			return colorGenerator.NextColor();
		})
		.style("opacity", "0.7")
		.attr("stroke", "black");
}

function getYearRange(works) {
	if(works.length === 0) return [0, 0];
	let min = works[0].year, max = works[0].year;

	works.forEach(function (work) {
		if(work.year < min) min = work.year;
		if(work.year > max) max = work.year;
	});

	return [min, max]
}

function getScoreRange(works) {
	if(works.length === 0) return [0, 0];
	var min = 0, max = 10;

	works.forEach(function (work) {
		if(work.points < min) min = work.points;
		if(work.points > max) max = work.points;
	});

	return [min, max]
}

function filterByMinisterial(works, threshold) {
	return works.filter(function (work) {
		return work.points >= threshold
	})
}

function groupByYear(yearRange, works) {
	const years = [];
	let wIdx = 0;
	for (let year = yearRange[0]; year < yearRange[1]; year++) {
		let groupedPoints = 0,
			groupedTitles = [];
		while(works[wIdx] && works[wIdx].year === year) {
			groupedPoints += works[wIdx].points;
			groupedTitles.push(works[wIdx].title);
			wIdx++;

		}

		years.push({
			year: year,
			points: groupedPoints,
			titles: groupedTitles
		})
	}

	return years
}

function toDate(year){
	return new Date(year, 0, 1)
}



