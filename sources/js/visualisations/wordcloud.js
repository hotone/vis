import * as $ from 'jquery'
import * as d3 from 'd3'
import * as cloud from 'd3-cloud'
import {InjectTranslations} from "../common";

document.addEventListener('DOMContentLoaded', () => InjectTranslations((strings) => {
    const svg = d3.select('#svg-port');
    const width = parseInt(svg.style('width').replace('px', ''));
    const height = parseInt(svg.style('height').replace('px', ''));
    let wordCloud;
    $.post("wordcloudData", function (data) {
        wordCloud = new Wordcloud(data, width, height);

        const $buttonHost = document.querySelector('#genPdfBtn').parentElement;

        const slider = document.createElement('input');
        slider.setAttribute('type', 'range');
        slider.setAttribute('style', 'border: none');
        slider.setAttribute('min', '1');
        slider.setAttribute('max', wordCloud.maxWordFrequency);
        slider.setAttribute('step', '1');
        slider.setAttribute('id', 'keywords-slider');
        slider.value = wordCloud.wordsFrequencyThreshold
        slider.onchange = () => {
            sliderDisplay.innerText = `${strings.word_clouds.threshold}: ${slider.value}`
            wordCloud.updateWordCloudForThreshold(slider.value)
        }

        const sliderDisplay = document.createElement('p');
        sliderDisplay.setAttribute('id', 'slider-display');
        sliderDisplay.innerText = `${strings.word_clouds.threshold}: ${slider.value}`

        const sliderDiv = document.createElement('div');
        sliderDiv.setAttribute('class', 'row');
        sliderDiv.appendChild(sliderDisplay);
        sliderDiv.appendChild(slider);

        const keywordsLanguageSelect = document.createElement('select')
        keywordsLanguageSelect.setAttribute('id', 'keywords_language_select')
        let defaultOption = new Option(strings.word_clouds.keywords_language, '');
        defaultOption.setAttribute('selected', '');
        defaultOption.setAttribute('disabled', '')
        keywordsLanguageSelect.append(defaultOption);
        keywordsLanguageSelect.append(new Option(strings.word_clouds.polish_keywords, 'polish'));
        keywordsLanguageSelect.append(new Option(strings.word_clouds.english_keywords, 'english'));
        keywordsLanguageSelect.onchange = () => {
            wordCloud.updateWordCloudForLanguage(keywordsLanguageSelect.value)
        }

        const colorSchemeSelect = document.createElement('select')
        colorSchemeSelect.setAttribute('id', 'color_scheme_select')
        let colorSchemeSelectDefaultOption = new Option(strings.word_clouds.color_scheme, '');
        colorSchemeSelectDefaultOption.setAttribute('selected', '');
        colorSchemeSelectDefaultOption.setAttribute('disabled', '')
        colorSchemeSelect.append(colorSchemeSelectDefaultOption);
        colorSchemeSelect.append(new Option(strings.word_clouds.color, 'color'));
        colorSchemeSelect.append(new Option(strings.word_clouds.mono, 'mono'));
        colorSchemeSelect.onchange = () => {
            wordCloud.updateWordCloudColorScheme(colorSchemeSelect.value)
        }

        $buttonHost.appendChild(sliderDiv);
        $buttonHost.appendChild(keywordsLanguageSelect);
        $buttonHost.appendChild(colorSchemeSelect);
    })
}));

class Wordcloud {
    constructor(data, width, height) {
        this.colorScheme = 'color';
        this.keywordsLanguage = "polish";
        this.wordsFrequencyThreshold = 1;
        this.width = width;
        this.height = height;
        this.allWords = data;
        this.displayedWords = this.changeRangeOfWordsByThreshold(data, this.wordsFrequencyThreshold);
        this.createWordcloudLayout(this.displayedWords);
    }

    changeRangeOfWordsByThreshold(data, threshold) {
        let displayedWords = [];
        if (this.keywordsLanguage === "polish") {
            this.maxWordFrequency = this.getMaxKeywordsFrequency(data.allPolishKeywords);
            displayedWords = this.getKeywordsWithThreshold(threshold, data.allPolishKeywords)
        } else if (this.keywordsLanguage === "english") {
            this.maxWordFrequency = this.getMaxKeywordsFrequency(data.allEnglishKeywords);
            displayedWords = this.getKeywordsWithThreshold(threshold, data.allEnglishKeywords)
        }
        return displayedWords;
    }

    createWordcloudLayout(words) {
        let colorScale;
        if(this.colorScheme === 'color'){
            colorScale = d3.scaleOrdinal(d3.schemeDark2)
        }
        if (this.colorScheme === 'mono'){
            colorScale = d3.scaleOrdinal(d3.schemeBlues)
        }

        let layout = cloud()
            .size([this.width, this.height])
            .words(words)
            .padding(5)
            .spiral("rectangular")
            .font("Impact")
            .rotate(0)
            .on("end", displayWordCloud)
        layout.start();

        function displayWordCloud(data) {
            const svg = d3.select('#svg-port');

            svg.append("g")
                .attr("transform", "translate(" + layout.size()[0] / 2 + "," + layout.size()[1] / 2 + ")")
                .selectAll("text")
                .data(data)
                .enter()
                .append("text")
                .style("font-size", function (word) {
                    return word.size + "px";
                })
                .style("fill", (d, i) => colorScale(i))
                .attr("text-anchor", "middle")
                .attr("transform", function (word) {
                    return "translate(" + [word.x, word.y] + ")rotate(" + word.rotate + ")";
                })
                .text(function (d) {
                    return d.text;
                });
        }
    }

    updateWordCloudColorScheme(colorScheme){
        this.colorScheme = colorScheme;
        d3.selectAll('#svg-port > *').remove();
        this.displayedWords = this.changeRangeOfWordsByThreshold(this.allWords, this.wordsFrequencyThreshold);
        this.createWordcloudLayout(this.displayedWords);
    }

    updateWordCloudForThreshold(threshold){
        this.wordsFrequencyThreshold = threshold;
        d3.selectAll('#svg-port > *').remove();
        this.displayedWords = this.changeRangeOfWordsByThreshold(this.allWords, threshold);
        this.createWordcloudLayout(this.displayedWords);
    }

    updateWordCloudForLanguage(language){
        this.keywordsLanguage = language
        d3.selectAll('#svg-port > *').remove();
        this.displayedWords = this.changeRangeOfWordsByThreshold(this.allWords, this.wordsFrequencyThreshold);
        document.getElementById('keywords-slider').setAttribute('max', this.maxWordFrequency);
        this.createWordcloudLayout(this.displayedWords);
    }

    getKeywordsWithThreshold(threshold = 2, wordList) {
        let keywordsQualified = []
        wordList.forEach(word => {
            if (word.value >= threshold) {
                keywordsQualified.push({text: word.text, value: word.value * 150})
            }

        })
        return keywordsQualified
    }

    getMaxKeywordsFrequency(wordList){
        let maxFrequency = 1
        wordList.forEach(word => {
            if(word.value > maxFrequency){
                maxFrequency = word.value
            }
        })
        return maxFrequency
    }
}



