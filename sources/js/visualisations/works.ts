import * as d3 from 'd3';
import {getDimensions} from "../util";
import d3Tip from 'd3-tip';
import {FetchCurrentWorks, FetchScriptableStrings} from "../common";

class YearData {
    constructor(
        public works: number,
        public year: number,
        public articles: number,
        public books: number,
        public chapters: number,
        public other: number,
        public titles: Array<string>,
        public articlesTitles: Array<string>,
        public booksTitles: Array<string>,
        public chaptersTitles: Array<string>,
        public otherTitles: Array<string>
    ) {
    }
}

document.addEventListener('DOMContentLoaded', async function () {
    const works = await FetchCurrentWorks();
    const jsStrings = await FetchScriptableStrings();
    let barData = getBars(works);
    drawWorksOverTimeGraph(barData, jsStrings);
});

function drawWorksOverTimeGraph(data: Array<YearData>, jsStrings) {
    const margin = {top: 50, right: 50, bottom: 50, left: 50}

    const bookColor = "#ff595e";
    const chapterColor = "#ffca3a";
    const articleColor = "#8ac926";
    const otherColor = "#1982c4";

    const svg = d3.select("#svg-port");
    const [width, height] = getDimensions('#svg-port');
    svg.selectAll('*').remove();
    const barYearTip = d3Tip().attr('class', 'd3-tip')['html'](function (d) {
        return `${jsStrings.vis.year["1"]} ${d.year}` +
            `</br>${jsStrings.vis.work_amount}: ${d.works}` +
            `</br>${jsStrings.vis.tooltip.book}: ${d.books}` +
            `</br>${jsStrings.vis.tooltip.chapter_in_book}: ${d.chapters}` +
            `</br>${jsStrings.vis.tooltip.article_or_review}: ${d.articles}` +
            `</br>${jsStrings.vis.tooltip.other}: ${d.other}`;
    });
    svg.call(barYearTip);

    const maxBarHeight = height - margin.top;
    const maxBarWidth = width - margin.right;
    let maxWorksAmountPerYear = d3.max(data, function (d) {
        return Number.parseInt(d.works.toString())
    })
    maxWorksAmountPerYear = maxWorksAmountPerYear + Math.round(maxWorksAmountPerYear * 0.3)
    const maxYearRangeForAxis = data.map(function (el) {
        return el.year.toString();
    })

    const yScaleWidth = 30;

    const xScale = d3.scaleBand()
        .domain(maxYearRangeForAxis)
        .range([0, maxBarWidth - yScaleWidth])
        .paddingInner(0.1)
        .paddingOuter(0.2)

    const yScale = d3.scaleLinear()
        .domain([0, maxWorksAmountPerYear])
        .range([0, maxBarHeight]);

    if (svg.select('g').empty()) svg.append('g').attr('id', 'bars');
    const bars = svg.select('g')
        .selectAll('rect')
        .data(data);

    bars.enter()
        .append('rect')
        .attr('width', xScale.bandwidth())
        .attr('x', function (d) {
            return xScale(d.year.toString());
        })
        .attr('y', maxBarHeight)
        .attr('height', 0)
        .style('fill', '#FFFFFF')
        .attr('transform', `translate(${[yScaleWidth, yScaleWidth]})`)
        .on('mouseover', barYearTip.show)
        .on('mouseout', barYearTip.hide)
        .transition()
        .duration(1000)
        .attr('y', function (d) {
            return maxBarHeight - yScale(d.books);
        })
        .attr('height', function (d) {
            return yScale(d.books);
        })
        .style('fill', bookColor);

    bars.enter()
        .append('rect')
        .attr('width', xScale.bandwidth())
        .attr('x', function (d) {
            return xScale(d.year.toString());
        })
        .attr('y', maxBarHeight)
        .attr('height', 0)
        .style('fill', '#FFFFFF')
        .attr('transform', `translate(${[yScaleWidth, yScaleWidth]})`)
        .on('mouseover', barYearTip.show)
        .on('mouseout', barYearTip.hide)
        .transition()
        .duration(1000)
        .attr('y', function (d) {
            if (d.books > 0) return maxBarHeight - yScale(d.books) - yScale(d.chapters);
            else return maxBarHeight - yScale(d.chapters)
        })
        .attr('height', function (d) {
            return yScale(d.chapters);
        })
        .style('fill', chapterColor);

    bars.enter()
        .append('rect')
        .attr('width', xScale.bandwidth())
        .attr('x', function (d) {
            return xScale(d.year.toString());
        })
        .attr('y', maxBarHeight)
        .attr('height', 0)
        .style('fill', '#FFFFFF')
        .attr('transform', `translate(${[yScaleWidth, yScaleWidth]})`)
        .on('mouseover', barYearTip.show)
        .on('mouseout', barYearTip.hide)
        .transition()
        .duration(1000)
        .attr('y', function (d) {
            let finalHeight = maxBarHeight;
            if (d.books > 0) finalHeight -= yScale(d.books);
            if (d.chapters > 0) finalHeight -= yScale(d.chapters);
            return finalHeight - yScale(d.articles);
        })
        .attr('height', function (d) {
            return yScale(d.articles);
        })
        .style('fill', articleColor);

    bars.enter()
        .append('rect')
        .attr('width', xScale.bandwidth())
        .attr('x', function (d) {
            return xScale(d.year.toString());
        })
        .attr('y', maxBarHeight)
        .attr('height', 0)
        .style('fill', '#FFFFFF')
        .attr('transform', `translate(${[yScaleWidth, yScaleWidth]})`)
        .on('mouseover', barYearTip.show)
        .on('mouseout', barYearTip.hide)
        .transition()
        .duration(1000)
        .attr('y', function (d) {
            let finalHeight = maxBarHeight;
            if (d.books > 0) finalHeight -= yScale(d.books);
            if (d.chapters > 0) finalHeight -= yScale(d.chapters);
            if (d.articles > 0) finalHeight -= yScale(d.articles);
            return finalHeight - yScale(d.other);
        })
        .attr('height', function (d) {
            return yScale(d.other);
        })
        .style('fill', otherColor);

    bars.exit()
        .transition()
        .duration(300)
        .attr('width', 0)
        .remove();

    const xAxis = d3.axisBottom(xScale);
    svg.append('g')
        .attr('id', 'x-axis')
        .attr("stroke", "black")
        .call(xAxis)
        .attr('transform', 'translate( ' + [-width, height / 2] + ')')
        .transition()
        .duration(1000)
        .attr('transform', 'translate( ' + [yScaleWidth, height - 20] + ')');

    const yAxis = d3.axisLeft(yScale.domain(yScale.domain().reverse()));
    svg.append('g')
        .attr('id', 'y-axis')
        .call(yAxis)
        .attr('transform', 'translate( ' + [0, height * 2] + ')')
        .transition()
        .duration(1000)
        .attr('transform', 'translate( ' + [30, height - maxBarHeight - 20] + ')')
        .attr("stroke", "black");

    let colorsLegend = [
        {"text": jsStrings.vis.tooltip.book, "color": bookColor},
        {"text": jsStrings.vis.tooltip.chapter_in_book, "color": chapterColor},
        {"text": jsStrings.vis.tooltip.article_or_review, "color": articleColor},
        {"text": jsStrings.vis.tooltip.other, "color": otherColor},
    ]

    let legend = svg.append('g')
        .attr('class', 'legend')
        .attr('transform', `translate(50, 25)`);

    legend.selectAll('rect')
        .data(colorsLegend)
        .enter()
        .append('rect')
        .attr('x', 0)
        .attr('y', function (d, i) {
            return (i+1) * 18;
        })
        .attr('width', 12)
        .attr('height', 12)
        .attr('fill', function (d, i) {
            return d.color;
        });

    legend.selectAll('text')
        .data(colorsLegend)
        .enter()
        .append('text')
        .text(function (d) {
            return d.text;
        })
        .attr('x', 18)
        .attr('y', function (d, i) {
            return (i+1) * 18;
        })
        .attr('text-anchor', 'start')
        .attr('alignment-baseline', 'hanging');
}

function countYearOccurrences(year, arr): YearData {
    const yearData = new YearData(0, year, 0, 0, 0, 0, [], [], [], [], []);
    arr.forEach(function (work) {
        if (work.year === year) {
            yearData.works++;
            yearData.titles.push(work.title);
            if (work.publicationCharacteristics === 'book') {
                yearData.books++;
                yearData.booksTitles.push(work.title)
            } else if (work.publicationCharacteristics === 'chapter_in_book') {
                yearData.chapters++;
                yearData.chaptersTitles.push(work.title);
            } else if (work.publicationCharacteristics === 'article_or_review') {
                yearData.articles++;
                yearData.articlesTitles.push(work.title);
            } else if (work.publicationCharacteristics === 'other') {
                yearData.other++;
                yearData.otherTitles.push(work.title);
            } else {
                yearData.other++;
                yearData.otherTitles.push(work.title);
            }
        }
    });
    return yearData;
}

function getYearRange(works) {
    if (works.length === 0) return [0, 0];
    let min = works[0].year, max = works[0].year;

    works.forEach(function (work) {
        if (work.year < min) min = work.year;
        if (work.year > max) max = work.year;
    });

    return [min, max]
}

function getBars(allWorks): Array<YearData> {
    const yearRange = getYearRange(allWorks);
    const beginningYear = yearRange[0];
    const endYear = yearRange[1];

    const bars = [];

    for (let year = beginningYear; year <= endYear; year++) {
        bars.push(countYearOccurrences(year, allWorks));
    }

    return bars;
}
