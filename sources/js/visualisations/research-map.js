import * as d3 from 'd3';
import d3Tip from 'd3-tip';
import {InjectContext} from "../common";
import {Domains} from '../domains';
import {Disciplines} from '../disciplines'


document.addEventListener('DOMContentLoaded', () => InjectContext( (works, strings) => {

    let visibilities = {
        disciplinesBubbles: {
            state: "hidden"
        },
        disciplinesHullArea: {
            state: "hidden"
        },
        userArticlesBubbles: {
            state: "visible"
        },
        userArticlesHullArea: {
            state: "hidden"
        }
    }

    const pdfGenerationButton = document.querySelector('#genPdfBtn').parentElement;

    const slider = document.createElement('input');
    slider.setAttribute('type', 'range');
    slider.setAttribute('style', 'border: none');
    slider.setAttribute('min', '0.01');
    slider.setAttribute('max', '1.0');
    slider.setAttribute('step', '0.01');
    slider.setAttribute('id', 'keywords-slider');
    slider.value = '1.0';
    slider.onchange = () => {
        sliderDisplay.innerText = `${strings.bubbles.scale_factor}: ${Math.fround(parseFloat(slider.value) * 100)}%`
        d3.selectAll('#svg-port > *').remove();
        drawGraph(works, strings, parseFloat(slider.value), visibilities);
    }

    const sliderDisplay = document.createElement('p');
    sliderDisplay.setAttribute('id', 'slider-display');
    sliderDisplay.innerText = `${strings.bubbles.scale_factor}: ${Math.fround(parseFloat(slider.value) * 100)}%`

    const sliderDiv = document.createElement('div');
    sliderDiv.setAttribute('class', 'row');
    sliderDiv.appendChild(sliderDisplay);
    sliderDiv.appendChild(slider);

    const dhsiPublicationPercentageDisplay = document.createElement('p');
    dhsiPublicationPercentageDisplay.setAttribute('id', 'dhsi-percentage-display');
    dhsiPublicationPercentageDisplay.innerText = `${strings.researchMap.masiPercentageLabel}: -`

    const dhsiDisplay = document.createElement('p');
    dhsiDisplay.setAttribute('id', 'dhsi-display');
    dhsiDisplay.innerText = `${strings.researchMap.masiLabel}: -`

    const dhsiDiv = document.createElement('div');
    dhsiDiv.setAttribute('class', 'row');
    dhsiDiv.appendChild(dhsiDisplay);
    dhsiDiv.appendChild(dhsiPublicationPercentageDisplay);

    const masiButton = document.createElement('button');
    masiButton.setAttribute('id','show-masi-button');
    masiButton.setAttribute("state", "show")
    masiButton.setAttribute('class','btn line-btn');

    const span = document.createElement('span');
    span.innerHTML = strings.researchMap.showMasiAreaButtonText
    masiButton.appendChild(span);
    masiButton.onclick = function (){
        if( masiButton.getAttribute("state") === "show"){
            document.getElementById("show-articles-button").disabled = true;
            document.getElementById("reference-area-button").disabled = true;

            masiButton.innerText =  strings.researchMap.hideMasiAreaButtonText
            masiButton.setAttribute("state", "hide")

            if (document.getElementById('show-articles-button').getAttribute("state") == "hide"){
                visibilities.disciplinesHullArea.state = "visible"
                document.getElementById("all-articles-area").setAttribute("visibility", visibilities.disciplinesHullArea.state)
            }
            else{
                visibilities.userArticlesHullArea.state = "visible"
                document.getElementById("masi-area").setAttribute("visibility", visibilities.userArticlesHullArea.state)
            }

        } else if(masiButton.getAttribute("state") === "hide"){
            document.getElementById("show-articles-button").disabled = false;
            document.getElementById("reference-area-button").disabled = false;

            masiButton.innerText =  strings.researchMap.showMasiAreaButtonText
            masiButton.setAttribute("state", "show")

            if (document.getElementById('show-articles-button').getAttribute("state") == "hide") {
                visibilities.disciplinesHullArea.state = "hidden"
                document.getElementById("all-articles-area").setAttribute("visibility", visibilities.disciplinesHullArea.state)
            }
            else{
                visibilities.userArticlesHullArea.state = "hidden"
                document.getElementById("masi-area").setAttribute("visibility", visibilities.userArticlesHullArea.state)
            }
        }
    }    
    
    const disciplinesMapButton = document.createElement('button');
    disciplinesMapButton.setAttribute('id','show-articles-button');
    disciplinesMapButton.setAttribute("state", "show")
    disciplinesMapButton.setAttribute('class','btn line-btn');

    const disciplinesMapButtonSpan = document.createElement('span');
    disciplinesMapButtonSpan.innerHTML = strings.researchMap.showDisciplinesButtonText
    disciplinesMapButton.appendChild(disciplinesMapButtonSpan);
    disciplinesMapButton.onclick = function (){
        if( disciplinesMapButton.getAttribute("state") === "show"){
            visibilities.disciplinesBubbles.state = "visible"
            visibilities.userArticlesBubbles.state = "hidden"

            disciplinesMapButton.innerText = strings.researchMap.hideDisciplinesButtonText
            disciplinesMapButton.setAttribute("state", "hide")

            document.querySelectorAll('[type="article-bubble"]').forEach(element => element.setAttribute("visibility", visibilities.disciplinesBubbles.state))
            document.querySelectorAll('[type="user-bubble"]').forEach(element => element.setAttribute("visibility", visibilities.userArticlesBubbles.state))

        } else if(disciplinesMapButton.getAttribute("state") === "hide"){
            visibilities.disciplinesBubbles.state = "hidden"
            visibilities.userArticlesBubbles.state = "visible"

            disciplinesMapButton.innerText = strings.researchMap.showDisciplinesButtonText
            disciplinesMapButton.setAttribute("state", "show")

            document.querySelectorAll('[type="article-bubble"]').forEach(element => element.setAttribute("visibility", visibilities.disciplinesBubbles.state))
            document.querySelectorAll('[type="user-bubble"]').forEach(element => element.setAttribute("visibility", visibilities.userArticlesBubbles.state))
        }
    }

    const referenceMapButton = document.createElement('button');
    referenceMapButton.setAttribute('id','reference-area-button');
    referenceMapButton.setAttribute("state", "show")
    referenceMapButton.setAttribute('class','btn line-btn');

    const referenceMapButtonSpan = document.createElement('span');
    referenceMapButtonSpan.innerHTML = strings.researchMap.showReferenceAreaButtonText
    referenceMapButton.appendChild(referenceMapButtonSpan);
    referenceMapButton.onclick = function (){
        if( referenceMapButton.getAttribute("state") === "show"){
            document.getElementById("show-articles-button").disabled = true;
            document.getElementById("show-masi-button").disabled = true;

            referenceMapButton.innerHTML = strings.researchMap.hideReferenceAreaButtonText
            referenceMapButton.setAttribute("state", "hide")

            visibilities.disciplinesHullArea.state = "visible"
            visibilities.userArticlesHullArea.state = "visible"

        } else if(referenceMapButton.getAttribute("state") === "hide"){
            document.getElementById("show-articles-button").disabled = false;
            document.getElementById("show-masi-button").disabled = false;

            referenceMapButton.innerHTML = strings.researchMap.showReferenceAreaButtonText
            referenceMapButton.setAttribute("state", "show")

            visibilities.disciplinesHullArea.state = "hidden"
            visibilities.userArticlesHullArea.state = "hidden"

        }
        drawGraph(works, strings, parseFloat(slider.value), visibilities);
    }

    sliderDiv.appendChild(dhsiDiv);

    pdfGenerationButton.appendChild(sliderDiv);

    pdfGenerationButton.appendChild(masiButton)
    
    pdfGenerationButton.appendChild(disciplinesMapButton)

    pdfGenerationButton.appendChild(referenceMapButton);

    drawGraph(works, strings, parseFloat(slider.value), visibilities);

}));

function drawGraph(works, strings, scaleFactor, visibilitiesStates){

    let amountOfComputedWorksForMasi = 0;
    let amountOfAllWorks = 0;

    let articlePointsOnGraph = []

    Object(works).forEach((work) => {
        amountOfAllWorks++;
        if (work.disciplines.length === 0){
            return;
        }
        else {
            amountOfComputedWorksForMasi ++;
            let articlePointOnGraph = {
                disciplinesList: []
            }

            Object(work.disciplines).forEach((discipline) => {
                let disciplineObj = null
                let disciplineName = null

                if (discipline.startsWith(" ") || discipline.endsWith(" ")){
                    disciplineName = discipline.trim()
                    disciplineObj = Disciplines[disciplineName]
                }
                else{
                    disciplineName = discipline
                    disciplineObj = Disciplines[disciplineName]
                }

                articlePointOnGraph.disciplinesList.push(
                    {
                        x: parseInt(disciplineObj["coordinates"]["X"]),
                        y: parseInt(disciplineObj["coordinates"]["Y"]),
                        color: Domains[disciplineObj["scientific_field"]],
                        amountOfJournals: parseInt(disciplineObj["amount_of_journals"]),
                        disciplineName: disciplineName,
                        scientificField: disciplineObj["scientific_field"],
                        weight: 1
                    }
                )
            })
            articlePointsOnGraph.push(articlePointOnGraph)
        }
    })

    Object(articlePointsOnGraph).forEach((article) => {

        let listOfCoordinates = []

        Object(article.disciplinesList).forEach((discipline) => {
            listOfCoordinates.push([discipline.x, discipline.y])
        })

        let hullCoordinates = d3.polygonHull(listOfCoordinates)

        let pointCoordinates

        if (listOfCoordinates.length > 2){
            pointCoordinates = d3.polygonCentroid(hullCoordinates)
            // if not calculating hull first
            // pointCoordinates = d3.polygonCentroid(listOfCoordinates)
        } else if (listOfCoordinates.length === 2){
            pointCoordinates = [
                 Math.floor((listOfCoordinates[0][0] + listOfCoordinates[1][0]) / 2),
                 Math.floor((listOfCoordinates[0][1] + listOfCoordinates[1][1]) / 2)
            ]
        } else {
            pointCoordinates = listOfCoordinates[0]
        }

        article.x = pointCoordinates[0]
        article.y = pointCoordinates[1]
        article.coordinates = pointCoordinates
    })

    let svg = d3.select('svg')
    svg.selectAll('*').remove();
    svg.attr("width", 850).attr("height", 850)

    d3.xml("images/research-map-background.svg")
        .then(async (xml) => {

            let retrieveColorsLegend = (keys => {
                let colors = []
                for (let key of keys) {
                    colors.push(
                        {
                            "text": strings.researchMap.scientificFields[key],
                            "color": Domains[key]
                        }
                    )
                }
                return colors
            })
            let colorsLegend = retrieveColorsLegend(Object.keys(Domains))

            let legend = svg.append('g')
                .attr('class', 'legend')
                .attr('transform', `translate(900, 25)`);

            legend.selectAll('rect')
                .data(colorsLegend)
                .enter()
                .append('rect')
                .attr('x', 0)
                .attr('y', function (d, i) {
                    return (i+1) * 18;
                })
                .attr('width', 12)
                .attr('height', 12)
                .attr('stroke', 'black')
                .attr('fill', function (d, i) {
                    return d.color;
                });

            legend.selectAll('text')
                .data(colorsLegend)
                .enter()
                .append('text')
                .text(function (d) {
                    return d.text;
                })
                .attr('x', 18)
                .attr('y', function (d, i) {
                    return (i+1) * 18;
                })
                .attr('text-anchor', 'start')
                .attr('alignment-baseline', 'hanging');

            // add miniature research map in right bottom corner

            let miniatureDisciplinesMapImageTitle = svg.append("text")
                .attr('x', 900)
                .attr('y', 545)
                .text(`${strings.researchMap.miniatureMapTitle}`)
                .attr('text-anchor', 'start')
                .attr('style', 'font-weight: bold')
                .attr('alignment-baseline', 'hanging');


            let miniatureDisciplinesMapImage = svg.append("image")
                .attr('xlink:href', "images/disciplines-miniature.png")
                .attr('width', 300)
                .attr('height', 300)
                .attr('x', 900)
                .attr('y', 550)
                .attr("style", "filter: drop-shadow(0px 0px 2px black);")

            let background = xml.documentElement
            background.setAttribute('x', '0')
            background.setAttribute('y', '0')
            background.setAttribute('width', '850')
            background.setAttribute('height', '850')
            background.setAttribute('opacity', '0.8')

            svg.node().appendChild(background)

            const tip = d3Tip()
                .attr('class', 'd3-tip')
                .html(function (disciplineName, disciplineProperties) {
                    return `Ilość czasopism: ${disciplineProperties.amount_of_journals}` +
                        `</br>Dyscyplina: ${disciplineName}` +
                        `</br>Dziedzina: ${disciplineProperties.scientific_field}`
                })
                .direction('nw')
                .offset([1, 1])
            svg.call(tip)

            const tipDot = d3Tip()
                .attr('class', 'd3-tip')
                .html(function (disciplineList) {

                    let disciplines = `${strings.researchMap.disciplines}:`

                    for (const discipline of disciplineList){
                        disciplines += `</br>- ${discipline.disciplineName}`
                    }

                    return disciplines
                })
                .direction('nw')
                .offset([1, 1])
            svg.call(tipDot)

            let disciplinesPoints = []

            for (let discipline of Object.keys(Disciplines)) {
                let entry = Disciplines[discipline.trim()]
                disciplinesPoints.push([Math.floor(parseInt(entry["coordinates"]["X"])), Math.floor(parseInt(entry["coordinates"]["Y"]))])
                svg.append('circle')
                    .attr('id', discipline.trim())
                    .attr('type', 'article-bubble')
                    .attr('cx', parseInt(entry["coordinates"]["X"]))
                    .attr('cy', parseInt(entry["coordinates"]["Y"]))
                    .attr('r', parseInt(entry["amount_of_journals"]) * 0.01 * scaleFactor)
                    .attr('stroke', 'black')
                    .attr('opacity', 1.0)
                    .attr('visibility', visibilitiesStates.disciplinesBubbles.state)
                    .on('mouseover', function () {
                        tip.show(discipline.trim(), entry, this)
                    })
                    .on('mouseout', function (){
                        tip.hide()
                    })
                    .attr('fill', Domains[entry["scientific_field"]])
            }

            let points = []

            for (const article of articlePointsOnGraph){
                points.push([Math.floor(article.x), Math.floor(article.y)])
                svg.append('circle')
                    .attr('type', 'user-bubble')
                    .attr('cx', Math.floor(article.x))
                    .attr('cy', Math.floor(article.y))
                    .attr('r', 10 * scaleFactor)
                    .attr('stroke', 'black')
                    .attr('opacity', 1.0)
                    .attr('visibility', visibilitiesStates.userArticlesBubbles.state)
                    .on('mouseover', function () {
                        for (let discipline of article.disciplinesList) {
                            document.getElementById(discipline.disciplineName).setAttribute("visibility", "visible")
                        }
                    })
                    .on('click', function (){
                        tipDot.show(article.disciplinesList, this)
                    })
                    .on('mouseout', function (){
                        tipDot.hide()
                        for (let discipline of article.disciplinesList){
                            document.getElementById(discipline.disciplineName).setAttribute("visibility", "hidden")
                        }
                    })
                    .attr('fill', "#263238")
            }

            const h = svg
                .append("path")
                .attr("id", "masi-area")
                .style("stroke", "grey")
                .style("fill-opacity", "0.5")
                .style("fill", "none")
                .attr("visibility", visibilitiesStates.userArticlesHullArea.state)

            const hAllArticles = svg
                .append("path")
                .attr("id", "all-articles-area")
                .style("stroke", "grey")
                .style("fill-opacity", "0.5")
                .style("fill", "none")
                .attr("visibility", visibilitiesStates.disciplinesHullArea.state)

            let maxPossiblePolygonHull = d3.polygonHull(disciplinesPoints)
            const spaceArea = d3.polygonArea(maxPossiblePolygonHull)

            let polygonHull = d3.polygonHull(points)
            let polygonArea = d3.polygonArea(polygonHull)
            let multidisciplinaryAreaOfScientificInterests = (polygonArea / spaceArea).toFixed(2)

            for (let i = 2; i <= maxPossiblePolygonHull.length; i++) {
                const visible = maxPossiblePolygonHull.slice(0, i);
                hAllArticles.attr("d", `M${visible.join("L")}Z`);
            }

            for (let i = 2; i <= polygonHull.length; i++) {
                const visible = polygonHull.slice(0, i);
                h.attr("d", `M${visible.join("L")}Z`);
            }

            hAllArticles.transition().style("fill", "lightblue");
            h.transition().style("fill", "lightgreen");

            let dhsiDisp = document.querySelector('#dhsi-display').innerHTML = `${strings.researchMap.masiLabel}: ${multidisciplinaryAreaOfScientificInterests}`

            let percentage = (amountOfComputedWorksForMasi / amountOfAllWorks).toFixed(2);
            document.querySelector('#dhsi-percentage-display').innerHTML = `${strings.researchMap.masiPercentageLabel}: ${percentage}`;

        })


}


function haveListsTheSameValues(a,b) {
    const containsAll = (arr1, arr2) =>
        arr2.every(arr2Item => arr1.includes(arr2Item))

    const sameMembers = (arr1, arr2) =>
        containsAll(arr1, arr2) && containsAll(arr2, arr1);

    return sameMembers(a, b)
}
