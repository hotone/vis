import {InjectContext} from "../common";

function initMap() {
	return L.map('map').setView([52.23, 20.92], 5);
}

document.addEventListener('DOMContentLoaded', () => InjectContext((works, strings, token) => {

	let map = initMap();
	L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
		attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
		maxZoom: 18,
		id: 'mapbox/streets-v11',
		tileSize: 512,
		zoomOffset: -1,
		accessToken: token
	}).addTo(map);

	let screenShooter = L.simpleMapScreenshoter().addTo(map)

	works = works.filter(function (work) {
		return !!work.latLng
	})

	const titles = {};
	const markers = new L.MarkerClusterGroup({
		averageCenter: true,
		zoomOnClick: true,
		minimumClusterSize: 1,
		singleMarkerMode: true
	});
	works.forEach(function (workObject, index) {
		titles[index] = workObject.title;
		const marker = L.marker([workObject.latLng.lat, workObject.latLng.lng], {
			title: index,
			map: map,
		})
		marker.bindPopup(`<b>Opublikowano tutaj artykuł: </b> <br/>${workObject.title} <br/>
						  <b>Przez:</b> <br>${workObject.authors.join(', ')} <br/>
						  <b>Czaspismo:</b> <br>${workObject.journalTitle === undefined ? "Brak danych" : workObject.journalTitle}`)
		markers.addLayer(marker);
		map.addLayer(markers);
	});

	document.getElementById('generatePdfMapBtn').onclick = function (){
		var $svgPort = $('#svg-port');
		screenShooter.takeScreen('image', {mimeType: 'image/jpeg'}).then(blob => {
			$.post('/genPdfMap',
				{
					image: blob,
					args: {
						type: "google-map",
						width: $svgPort.width(),
						height: $svgPort.height(),
						title: strings.vis_titles["google-map"],
						caption: strings.pdf_caption,
						caption_title: strings.pdf_caption_title,
						for: strings.for,
						authorName: "",
						orientation: 'landscape'
					}
				},
				function (response) {
					window.location = response
				},
				"text")
		}).catch(e => {
			console.error(e)
		})
	}


}));




