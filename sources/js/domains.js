export const Domains = {
    "społeczne": "#4472c4",
    "teologia": "#ffff00",
    "ścisłe-przyrodncze": "#74dcf4",
    "sztuka": "#ed7d31",
    "techniczne": "#ff66cc",
    "medyczne": "#ff0000",
    "humanistyczne": "#ffffff",
    "rolnicze": "#a9d18e"
}
