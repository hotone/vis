
$(document).ready(function () {

    var DetailedDescriptions = {
        collab: jsStrings.vis_details.collab,
        works: jsStrings.vis_details.works,
        bubbles: jsStrings.vis_details.bubbles,
        "google-map": jsStrings.vis_details["google-map"],
        wordcloud: jsStrings.vis_details.wordcloud,
        "research-map": jsStrings.vis_details["research-map"],
        general: jsStrings.vis_details.general
    };

    DisplayDetails = function(visName) {
        $('#details-host').text(DetailedDescriptions[visName])
    }

    GetDetailsText = function (visName){
        return DetailedDescriptions[visName]
    }

    document.querySelectorAll('.card-image').forEach(function (c) {
        c.addEventListener('mouseout', function (event) {
            DisplayDetails('general');
        })
    })

    DisplayDetails('general')

    $('.dropdown-trigger').dropdown();
});

var inputFocused = false;
var wasChangedControlValue = 0;

function SetInputFocused() {
    inputFocused = true;
}
function SetInputBlurred() {
    inputFocused = false;
}

function InputChanged() {
    wasChangedControlValue++;
    var thisId = wasChangedControlValue;
    setTimeout(function () {
        if (wasChangedControlValue == thisId) {
            QueryForSuggestions()
            wasChangedControlValue = 0;
        }
    }, 1500)
}

function OpenModal(){
    // Get the modal
    var modal = document.getElementById("myModal");

// Get the button that opens the modal
    var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal
    btn.onclick = function() {
        modal.style.display = "block";
    }

// When the user clicks on <span> (x), close the modal
    span.onclick = function() {
        modal.style.display = "none";
    }
}

function Export(){

    $.get('/statistics', function (statistics){

        var data = [
            [
                statistics.publishingBegginingYear,
                statistics.sumOfWorks,
                statistics.sumOfPoints,
                statistics.coauthorsAmount,
                statistics.impactFactor,
                statistics.conferencePapersAmount,
                statistics.openAccessPapersAmount,
                statistics.engToPlPapersRatio,
                statistics.conferencePapersRatio,
                statistics.openAccessPapersRatio,
                statistics.percentageOfComputedWorksForNationalityOfPapers,
                statistics.rangeOfNationalCooperation,
                statistics.rangeOfInternationalCooperation,
                statistics.coreCollaboration10,
                statistics.scatteringFactor,
                statistics.percentageOfComputedWorksForMasi,
                statistics.multidisciplinaryAreaOfScientificInterests
            ],
        ];

        var csv = 'old_art_year,publ_count,min_points,coauth_num,if_total,conf_p_number,oa_count,en_pl,' +
            'conf_p_ratio,oa_p_ratio,analyzed_col_range_record_percent,nat_col_range,intern_col_range,' +
            'core_col,scat_f_auth,analyzed_multi_record_percent,multi_f\n';
        data.forEach(function(row) {
            csv += row.join(',');
            csv += "\n";
        });
        var authorName = statistics.authorName.toLowerCase().replace(' ', '_');
        var hiddenElement = document.createElement('a');
        hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
        hiddenElement.target = '_blank';
        hiddenElement.download = `scientfic_visualiser_${authorName}.csv`;
        hiddenElement.click();

        document.getElementById("myModal").style.display = "none";
    })
}

function ComputeStatistics(data) {

    var stats = {
        "works-amount-display": '-',
        "coworkers-amount-display": '-',
        "ministerial-points-amount-display": '-',
        "mean-ministerial-points-amount-display": '-',
        "max-shared-works-amount-display": '-'
    }

    if(!data || !data.length) return;

    stats["works-amount-display"] = data.length;

    var beginYear = data[0].year, endYear = data[0].year;

    data.forEach(function (work) {
        if(work.year < beginYear) beginYear = work.year;
        if(work.year > endYear) endYear = work.year;
    })

    // stats["mean-works-amount-display"] = (data.length / (endYear - beginYear + 1)).toFixed(1);

    var authorMap = {}, mostShared = {amount: -1, name: "-"}, mostSharedNotSelf = {amount: -1, name: "-"};

    data.forEach(function (work) {
        work.authors.forEach(function (author) {
            authorMap[author] = authorMap[author] ? authorMap[author]++ : 1;
        })
    });

    stats["coworkers-amount-display"] = Object.keys(authorMap).length;

    stats["ministerial-points-amount-display"] = data.map(d => d.points).reduce((a, b) => a + b);

    var firstWithPoints = data.find(d => d.points > 0);

    stats["mean-ministerial-points-amount-display"] = firstWithPoints ?
        (stats["ministerial-points-amount-display"] / (endYear - firstWithPoints.year + 1)).toFixed(2) :
        '-';


    for(var name in authorMap)
        if(authorMap.hasOwnProperty(name)) {
            if(authorMap[name] > mostShared.amount) {
                mostShared.amount = authorMap[name];
                mostShared.name = name;
            }
        }

    for(var secondName in authorMap)
        if(authorMap.hasOwnProperty(secondName)) {
            if(authorMap[secondName] > mostSharedNotSelf.amount && secondName !== mostShared.name) {
                mostSharedNotSelf.amount = authorMap[name];
                mostSharedNotSelf.name = name;
            }
        }


    stats["max-shared-works-amount-display"] = mostSharedNotSelf.name;


    for (var title in stats)
        if(stats.hasOwnProperty(title)) {
            $('#' + title).text(stats[title]);
        }

    return stats;
}


$.post("collabData", {}, ComputeStatistics)

var DisplayDetails;
var GetDetailsText;
